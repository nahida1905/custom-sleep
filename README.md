# custom sleep

Custom sleep is a version of the time.Sleep function that 
can be interrupted. When interrupted the function will return false, otherwise when the duration expires it will return true.

- go version: 1.17

