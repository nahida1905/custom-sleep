package main

import (
	"fmt"
	"time"
)

// interrupt send data to signal channel based on action result
// it will send empty struct to channel just for cancellation purpose
// for testing purpose we decided on canInterrupt parameters for cancellation
func interrupt(canInterrupt bool, signal chan struct{}) {
	//we don't have any actions due to we added time.Sleep just for waiting a little
	time.Sleep(100 * time.Millisecond)
	//do some actions
	if canInterrupt {
		signal <- struct{}{}
	}
}

func sleep(duration int, signal chan struct{}) bool {
	select {
	case <-time.After(time.Duration(duration) * time.Millisecond):
		fmt.Println("time expired")
		return true
	case <-signal:
		fmt.Println("got cancellation signal")
		return false
	}
}

func main() {
	signal := make(chan struct{})
	go interrupt(true, signal)

	result := sleep(300, signal)

	fmt.Println("result:", result)
}
