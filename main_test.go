package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_sleep(t *testing.T) {
	type args struct {
		duration     int
		signal       chan struct{}
		canInterrupt bool
	}

	testCases := []struct {
		args
		result bool
	}{
		{
			args: args{
				duration:     50,
				signal:       make(chan struct{}),
				canInterrupt: true,
			},
			result: true,
		},
		{
			args: args{
				duration:     50,
				signal:       make(chan struct{}),
				canInterrupt: false,
			},
			result: true,
		},
		{
			args: args{
				duration:     150,
				signal:       make(chan struct{}),
				canInterrupt: true,
			},
			result: false,
		},
		{
			args: args{
				duration:     150,
				signal:       make(chan struct{}),
				canInterrupt: false,
			},
			result: true,
		},
	}

	a := assert.New(t)
	for _, testCase := range testCases {

		go interrupt(testCase.canInterrupt, testCase.signal)

		result := sleep(testCase.duration, testCase.signal)

		a.Equal(testCase.result, result)

	}

}
